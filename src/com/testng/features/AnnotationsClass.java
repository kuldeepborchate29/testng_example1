package com.testng.features;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class AnnotationsClass {

	@BeforeClass
	public void beforeClass() {
		System.out.println("In Before Class");
	}

	@BeforeMethod
	public void beforeMethod() {
		System.out.println("In Before Method");
	}

	@AfterMethod
	public void afterMethod() {
		System.out.println("In After Method");
	}

	@AfterClass
	public void afterClass() {
		System.out.println("In After Class");
	}
	
	@BeforeTest
	public void beforeTest() {
		System.out.println("In Before test");
	}
	
	@AfterTest
	public void afterTest() {
		System.out.println("In After test");
	}

	@BeforeSuite
	public void beforeSuite() {
		System.out.println("In Before suite");
	}
	
	@AfterSuite
	public void afterSuite() {
		System.out.println("In After Suite");
	}
}
