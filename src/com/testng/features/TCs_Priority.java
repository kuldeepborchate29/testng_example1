package com.testng.features;

import org.testng.annotations.Test;

public class TCs_Priority {
	@Test(priority = 0)
	public void test1() {
		System.out.println("I have executed test 1");
	}
	

	@Test(priority = 3)
	public void test2() {
		System.out.println("I have executed test 2");
	}

	@Test(priority = 1)
	public void test3() {
		System.out.println("I have executed test 3");
	}

	@Test(priority = 2)
	public void test4() {
		System.out.println("I have executed test 4");
	}
}
