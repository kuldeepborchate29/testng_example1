package com.testng.features;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Module2 extends AnnotationsClass {

	@Test
	public void VerifyHomeLogo() {
		System.out.println("In TC 3");
	}

	@Test
	public void VerifyHomeMenu() {
		System.out.println("In TC 4");
	}
}
