package com.testng.features;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;



public class Paramerization  {
	/**
	 *Here the values were came form xml file where in we have to put this class
	 *Refer Parameterization.xml file
	 *Like wise
	 *<suite name="Suite">
  			<test thread-count="5" name="Test">
  				<parameter name="ValueOne" value="100"></parameter>
  				<parameter name="ValueTwo" value="200"></parameter>
			    <classes>
			      <class name="com.sarinity.radioButton.Module1"/>
			    </classes>
		  </test> <!-- Test -->
		</suite> <!-- Suite -->
	 */
	@Parameters({"ValueOne","ValueTwo"})
	@Test
	public void test6(int ValueOne, int ValueTwo) {
		System.out.println(ValueOne+ValueTwo);
	}
}
