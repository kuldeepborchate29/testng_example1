package com.testng.features;

import org.testng.annotations.Test;

public class DependsOnOtherTC {
	
	/**
	 * Method LogOut is depends on two methods i.e OpenBrowser & SignIn
	 * And method VerifySignOut is depends on one method Logout
	 */
	@Test
	public void OpenBrowser() {
		System.out.println("Opening The Browser");
	}

	@Test(dependsOnMethods = { "SignIn", "OpenBrowser" })
	public void LogOut() {
		System.out.println("Logging Out");
	}

	@Test
	public void SignIn() {
		System.out.println("Signing In");
	}
	
	@Test(dependsOnMethods = { "LogOut"})
	public void VerifySignOut() {
		System.out.println("VerifySignOut");
		
	}
}
