package com.testng.features;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class TCsForCBT {

	@Test
	@Parameters("Browser")
	public void Test1(String browser) {
		WebDriver driver = null;

		switch (browser.toLowerCase()) {
		case "chrome":
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\kuldeep\\chromedriver_win32_98\\chromedriver.exe");
			driver = new ChromeDriver();
			break;
			
		case "firefox":
			System.setProperty("webdriver.gecko.driver", "");
			driver = new FirefoxDriver();
			break;
			
		case "ie":
			System.setProperty("webdriver.ie.driver", "C:\\Users\\kulde\\IE driver\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			break;
			
		case "safari":
			System.setProperty("webdriver.safari.driver", "");
			driver = new SafariDriver();
			break;
			
		default:
			Assert.assertTrue(false, "No browser found name " + browser);

		}

		driver.get("https://courses.letskodeit.com/practice");
	}

}
