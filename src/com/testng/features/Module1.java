package com.testng.features;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Module1 extends AnnotationsClass {
	
	@Test
	public void VerifyHomeLogo() {
		System.out.println("In TC 2");
	}

	@Test
	public void VerifyHomeMenu() {
		System.out.println("In TC 1");
	}

	

	/*
	 * @BeforeClass public void beforeClass() {
	 * System.out.println("In Before Class"); }
	 * 
	 * @AfterClass public void afterClass() { System.out.println("In After Class");
	 * }
	 * 
	 * @BeforeTest public void beforeTest() { System.out.println("In Before Test");
	 * }
	 * 
	 * @AfterTest public void afterTest() { System.out.println("In After Test"); }
	 * 
	 * @BeforeSuite public void beforeSuite() {
	 * System.out.println("In Before Suite"); }
	 * 
	 * @AfterSuite public void afterSuite() { System.out.println("In After Suite");
	 * }
	 */

}
